package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

const leftDirection = -1
const rightDirection = 1

const voting = 1
const leaderDistributing = 2

type Message struct {
	id          int
	direction   int
	messageType int
	count       uint
}

type ResultMessage struct {
	leaderID int
	count    uint
}

func node(id int, leftChannel, myChannel, rightChannel chan Message, doneChannel chan ResultMessage) {
	receivedLeaderFromLeft := false
	receivedLeaderFromRight := false

	leftChannel <- Message{id, leftDirection, voting, 1}
	rightChannel <- Message{id, rightDirection, voting, 1}

	for {
		aMessage := <-myChannel
		if aMessage.messageType == voting {
			if aMessage.id > id {
				if aMessage.direction == leftDirection {
					leftChannel <- Message{aMessage.id, leftDirection, voting, aMessage.count + 1}
				} else {
					rightChannel <- Message{aMessage.id, rightDirection, voting, aMessage.count + 1}
				}
			} else if aMessage.id == id {
				leftChannel <- Message{id, leftDirection, leaderDistributing, aMessage.count + 1}
				rightChannel <- Message{id, rightDirection, leaderDistributing, aMessage.count + 1}
			}
		} else {
			if aMessage.direction == leftDirection {
				receivedLeaderFromRight = true
				leftChannel <- Message{aMessage.id, leftDirection, leaderDistributing, aMessage.count + 1}
			} else {
				receivedLeaderFromLeft = true
				rightChannel <- Message{aMessage.id, rightDirection, leaderDistributing, aMessage.count + 1}
			}

			if receivedLeaderFromLeft && receivedLeaderFromRight {
				doneChannel <- ResultMessage{aMessage.id, aMessage.count}
				return
			}
		}
	}
}

func main() {
	nPtr := flag.Int("n", 0, "number of vertices in ring")
	flag.Parse()
	n := *nPtr

	rand.Seed(time.Now().UTC().UnixNano())

	ids := make([]int, n)
	for i := range ids {
		ids[i] = rand.Int()
		println(ids[i])
	}

	channels := make([]chan Message, n)
	for i := range channels {
		channels[i] = make(chan Message, n)
	}

	doneChannel := make(chan ResultMessage, n)

	for i := range channels {
		prev := (i - 1 + n) % n
		next := (i + 1) % n
		go node(ids[i], channels[prev], channels[i], channels[next], doneChannel)
	}

	println()

	for i := 0; i < n; i++ {
		value := <-doneChannel
		fmt.Println(value)
	}

}
